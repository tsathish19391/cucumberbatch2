package com.cts.automation.PageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cts.automation.base.BaseClass;

public class Login extends BaseClass{	
	public Login() 
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="username")
	public WebElement userNameTxt;
	
	@FindBy(id="password") 
	public WebElement passwordTxt;
	
	@FindBy(id="login") 
	public WebElement loginBtn;
	
	@FindBy(xpath="//a[text()='New User Register Here']") 
	public WebElement sigupLink;	
	
	public void login(String userName, String pwd)
	{
		userNameTxt.sendKeys(userName);
		passwordTxt.sendKeys(pwd);
		loginBtn.click();
	}
	
}
