package com.cts.automation.PageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cts.automation.base.BaseClass;

public class FBLogin extends BaseClass{

	
	public FBLogin()
	{
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(xpath="//a[contains(text(),'Create New Account')]")
	public WebElement newUserLink;
	
	
	@FindBy(xpath="//input[@name='firstname']")
	public WebElement firstNameTxt;
	
	@FindBy(xpath="//input[@name='lastname']")
	public WebElement surNameTxt;
	
	@FindBy(xpath="//input[@name='reg_email__']")
	public WebElement emailTxt;
	
	public void signUp(String firstName, String lastName, String email)
	{
		firstNameTxt.sendKeys(firstName);
		surNameTxt.sendKeys(lastName);
		emailTxt.sendKeys(email);
	}
	
	
}
