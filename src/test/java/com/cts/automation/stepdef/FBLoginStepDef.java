package com.cts.automation.stepdef;

import java.util.List;
import java.util.Map;

import org.junit.Assert;

import com.cts.automation.PageObjects.FBLogin;
import com.cts.automation.base.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;

public class FBLoginStepDef extends BaseClass{


	@Given("the user open FB")
	public void the_user_open_FB() {
		launchChrome();
		driver.get("https://www.facebook.com/");
		Assert.assertTrue(true);
	}

	@Given("the user clicks create new user link")
	public void the_user_clicks_create_new_user_link() {
		FBLogin fbLogin = new FBLogin();
		fbLogin.newUserLink.click();
		Assert.assertTrue(true);

	}

	@When("user enters the required below info")
	public void signUp(DataTable dataTable) 
	{
		List<Map<String, String>> allData = dataTable.asMaps(String.class,String.class);
		 //| firstname | lastname | email            |
		FBLogin fbLogin = new FBLogin();
		String firstName =allData.get(0).get("firstname");
		String lastName =allData.get(0).get("lastname");

		String email =allData.get(0).get("email");

		fbLogin.signUp(firstName, lastName, email);
		Assert.assertTrue(false);
	}

	





}
