package com.cts.automation.stepdef;

import com.cts.automation.PageObjects.Login;
import com.cts.automation.base.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AdactinLoginStepDef extends BaseClass{

	@Given("user launches chrome and navigates to url")
	public void launchAndNavigate() 
	{
		launchChrome();
		driver.get("https://adactinhotelapp.com/");
	
	}

	@When("user enters username as {string} and password as {string}")
	public void enterCredentials(String useName, String pwd)
	{
		Login loginObj = new Login();
		loginObj.login(useName, pwd);	
	}


@Then("login should be successful")
public void login_should_be_successful() {

	
	
	
}


	
	
	
	
}
