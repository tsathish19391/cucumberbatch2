package com.cts.automation.stepdef;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Tes {




	public static List<List<String>> readDate() throws IOException
	{

		//Object[][] allDataArr = 

		List<List<String>> outerLi = new ArrayList<List<String>>();
		FileInputStream fis = new FileInputStream("C:\\Users\\Sathish\\eclipse-workspace\\DemoWork\\src\\test\\resources\\TestData\\searchItems.xlsx");
		Workbook w = new XSSFWorkbook(fis);
		Sheet s = w.getSheet("Sheet1");
		int totalRows = s.getPhysicalNumberOfRows();
		int totalCols = s.getRow(0).getPhysicalNumberOfCells();



		for(int i =0; i<s.getPhysicalNumberOfRows();i++)
		{
			List<String> innerLi = new ArrayList<String>();

			Row r = s.getRow(i);
			for(int j=0; j<r.getPhysicalNumberOfCells(); j++ )
			{
				Cell c = r.getCell(j);
				String value = c.getStringCellValue();
				innerLi.add(value);

			}
			outerLi.add(innerLi);
		}

		return outerLi;
	}

	//outlerLi.get(0).get(3); List of List
	//List of Map
	//outerLi.get(1).get("price")




	public static List<Map<String, String>> readInfo() throws IOException
	{

		List<Map<String, String>> mainLi = new ArrayList<Map<String, String>>();
		FileInputStream fis = new FileInputStream("C:\\Users\\Sathish\\eclipse-workspace\\DemoWork\\src\\test\\resources\\TestData\\searchItems.xlsx");
		Workbook w = new XSSFWorkbook(fis);
		Sheet s = w.getSheet("Sheet1");
		for(int i =1; i<s.getPhysicalNumberOfRows();i++)
		{
			Map<String,String> innerMap = new LinkedHashMap<String, String>();
			Row r = s.getRow(i);
			for(int j=0; j<r.getPhysicalNumberOfCells(); j++ )
			{
				Cell c = r.getCell(j);
				String value = c.getStringCellValue();
				if(j==0)
					innerMap.put("itemname", value);
				else if(j==1)
					innerMap.put("price", value);

			}
			mainLi.add(innerMap);

		}

		return mainLi;
	}






	public static void main(String[] args) throws IOException {


		List<Integer> allPriceFromUILi = new ArrayList<Integer>();
		
		allPriceFromUILi.add(12);
		allPriceFromUILi.add(23);
		allPriceFromUILi.add(45);
		allPriceFromUILi.add(89);
		
		List<Integer> tempLi = new ArrayList<Integer>(allPriceFromUILi);

		Collections.sort(tempLi);
		
		if(allPriceFromUILi.equals(tempLi))
			System.out.println("Sorting functionality is working fine");
		else
			System.out.println("Sorting functionality is not working");
		
		
		System.out.println("hello");

	}
}
