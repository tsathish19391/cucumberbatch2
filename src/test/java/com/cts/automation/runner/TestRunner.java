package com.cts.automation.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;



//dryRun =false -> I t will execute the steps for which step definition is present.
//If step def is not found, it will stop excution and will generate the snippet for missings step

//dryRun = true -> It wont execute any step at all. It will simply generate the snippets for missings steps alone


@RunWith(Cucumber.class)
@CucumberOptions( features="src//test//resources",     dryRun=false,

glue="com.cts.automation.stepdef",    monochrome=true
		)
public class TestRunner {
	
	

}
