package com.cts.automation.runner;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions( 
		features="src//test//resources",     dryRun=false,
		glue="com.cts.automation.stepdef",    monochrome=true,
		tags= {"@FBSignup"},
		plugin= {"html:C:\\Users\\Sathish\\eclipse-workspace\\CucumberBatch2\\src\\test\\resources\\Output\\HtmlReports"}
		, strict=false)
public class FBTestRunner {

	
	
	
}
