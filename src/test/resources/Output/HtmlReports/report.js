$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/FBLogin.feature");
formatter.feature({
  "name": "Functionalities containing FB Login page",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@FBLoginPage"
    }
  ]
});
formatter.scenarioOutline({
  "name": "FB sign up",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FBSignup"
    },
    {
      "name": "@Sanity"
    },
    {
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "name": "the user open FB",
  "keyword": "Given "
});
formatter.step({
  "name": "the user clicks create new user link",
  "keyword": "And "
});
formatter.step({
  "name": "user enters the required below info",
  "keyword": "When ",
  "rows": [
    {
      "cells": [
        "firstname",
        "lastname",
        "email"
      ]
    },
    {
      "cells": [
        "\u003cFirstName\u003e",
        "\u003cLastName\u003e",
        "\u003cEmailId\u003e"
      ]
    }
  ]
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "FirstName",
        "LastName",
        "EmailId"
      ]
    },
    {
      "cells": [
        "sathsh",
        "thiru",
        "sathish@gmail.com"
      ]
    },
    {
      "cells": [
        "suresh",
        "kumar",
        "suresh@gmail.com"
      ]
    }
  ]
});
formatter.scenario({
  "name": "FB sign up",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FBLoginPage"
    },
    {
      "name": "@FBSignup"
    },
    {
      "name": "@Sanity"
    },
    {
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "name": "the user open FB",
  "keyword": "Given "
});
formatter.match({
  "location": "FBLoginStepDef.the_user_open_FB()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user clicks create new user link",
  "keyword": "And "
});
formatter.match({
  "location": "FBLoginStepDef.the_user_clicks_create_new_user_link()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user enters the required below info",
  "rows": [
    {
      "cells": [
        "firstname",
        "lastname",
        "email"
      ]
    },
    {
      "cells": [
        "sathsh",
        "thiru",
        "sathish@gmail.com"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "FBLoginStepDef.signUp(DataTable)"
});
formatter.result({
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat com.cts.automation.stepdef.FBLoginStepDef.signUp(FBLoginStepDef.java:46)\r\n\tat ✽.user enters the required below info(src/test/resources/features/FBLogin.feature:8)\r\n",
  "status": "failed"
});
formatter.scenario({
  "name": "FB sign up",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FBLoginPage"
    },
    {
      "name": "@FBSignup"
    },
    {
      "name": "@Sanity"
    },
    {
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "name": "the user open FB",
  "keyword": "Given "
});
formatter.match({
  "location": "FBLoginStepDef.the_user_open_FB()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user clicks create new user link",
  "keyword": "And "
});
formatter.match({
  "location": "FBLoginStepDef.the_user_clicks_create_new_user_link()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user enters the required below info",
  "rows": [
    {
      "cells": [
        "firstname",
        "lastname",
        "email"
      ]
    },
    {
      "cells": [
        "suresh",
        "kumar",
        "suresh@gmail.com"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "FBLoginStepDef.signUp(DataTable)"
});
formatter.result({
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat com.cts.automation.stepdef.FBLoginStepDef.signUp(FBLoginStepDef.java:46)\r\n\tat ✽.user enters the required below info(src/test/resources/features/FBLogin.feature:8)\r\n",
  "status": "failed"
});
});