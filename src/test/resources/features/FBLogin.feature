@FBLoginPage
Feature: Functionalities containing FB Login page

  @FBSignup @Sanity @Regression
  Scenario Outline: FB sign up
    Given the user open FB
    And the user clicks create new user link
    When user enters the required below info
      | firstname   | lastname   | email     |
      | <FirstName> | <LastName> | <EmailId> |

    Examples: 
      | FirstName | LastName | EmailId           |
      | sathsh    | thiru    | sathish@gmail.com |
      | suresh    | kumar    | suresh@gmail.com  |
